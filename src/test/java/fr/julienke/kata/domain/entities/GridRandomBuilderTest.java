package fr.julienke.kata.domain.entities;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class GridRandomBuilderTest {

    @Test
    void should_generate_grid_with_n_row_m_columns_and_n_m_cells() {
        //when
        Grid grid = new GridRandomBuilder().build(14, 13, 5);
        //then
        assertThat(grid.getRow()).isEqualTo(14);
        assertThat(grid.getColumn()).isEqualTo(13);
        assertThat(grid.getCells().size()).isEqualTo(14*13);
    }

    @ParameterizedTest
    @CsvSource({"0,2", "2,0", "-1,4", "4,-1"})
    void should_throw_exception_if_number_of_rows_or_columns_are_negative_or_zero(int numberOfRows,
                                                                                  int numberOfColumns) {
        //when
        assertThatThrownBy(() -> new GridRandomBuilder().build(numberOfRows, numberOfColumns, 1))
            .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void should_throw_exception_if_number_of_mines_greater_than_cells() {
        //when
        assertThatThrownBy(() -> new GridRandomBuilder().build(10, 10, 101))
            .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void should_generate_grid_with_p_cells_with_mines() {
        //when
        Grid grid = new GridRandomBuilder().build(3,3, 5);
        //then
        assertThat(grid.getRow()).isEqualTo(3);
        assertThat(grid.getColumn()).isEqualTo(3);
        assertThat(grid.getCells().size()).isEqualTo(9);
        long cellsWithMines = grid.getCells()
            .entrySet()
            .stream()
            .filter(cellEntry -> cellEntry.getValue().hasMine())
            .count();
        assertThat(cellsWithMines).isEqualTo(5);
        assertThat(grid.getNumberOfMines()).isEqualTo(5);
    }

}