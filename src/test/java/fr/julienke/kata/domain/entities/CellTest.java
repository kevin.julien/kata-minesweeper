package fr.julienke.kata.domain.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

public class CellTest {

    Position cellPosition;

    @BeforeEach
    void setUp() {
        cellPosition =  new Position(4,14);
    }

    @Test
    void should_create_a_cell_with_a_fixed_position() {
        Cell aCell = new Cell(cellPosition, true);
        assertThat(aCell.getPosition().getRow()).isEqualTo(4);
        assertThat(aCell.getPosition().getColumn()).isEqualTo(14);
    }

    @Test
    void should_create_cell_with_a_mine() {
        Cell aCell = new Cell(cellPosition, true);
        assertThat(aCell.hasMine()).isTrue();
    }

    @Test
    void should_create_a_cell_with_no_mine() {
        Cell aCell = new Cell(cellPosition, false);
        assertThat(aCell.hasMine()).isFalse();
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void should_get_state_HIDDEN_after_creation_of_a_cell(boolean hasAMine) {
        Cell aCell = new Cell(cellPosition, hasAMine);
        assertThat(aCell.getState()).isEqualTo(CellState.HIDDEN);
    }

    @Test
    void should_get_state_UNCOVERED_after_uncover_cell_with_no_mine() {
        Cell aCell = new Cell(cellPosition, false);
        aCell.uncover();
        assertThat(aCell.getState()).isEqualTo(CellState.UNCOVERED);
    }

    @Test
    void should_get_state_EXPLODED_after_uncover_cell_with_a_mine() {
        Cell aCell = new Cell(cellPosition, true);
        aCell.uncover();
        assertThat(aCell.getState()).isEqualTo(CellState.EXPLODED);
    }
}
