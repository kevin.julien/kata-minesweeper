package fr.julienke.kata.domain.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class UncoverCellTest {

    Map<Position, Cell> cells;

    Grid grid;

    /**
     * Create a grid like that :
     * [ ] [ ] [ ]
     * [ ] [ ] [ ]
     * [ ] [*] [*]
     */
    @BeforeEach
    void setUp() {
        cells = new HashMap<>();
        cells.put(new Position(0, 0), new Cell(new Position(0, 0), false));
        cells.put(new Position(0, 1), new Cell(new Position(0, 1), false));
        cells.put(new Position(0, 2), new Cell(new Position(0, 2), false));

        cells.put(new Position(1, 0), new Cell(new Position(1, 0), false));
        cells.put(new Position(1, 1), new Cell(new Position(1, 1), false));
        cells.put(new Position(1, 2), new Cell(new Position(1, 2), false));

        cells.put(new Position(2, 0), new Cell(new Position(2, 0), false));
        cells.put(new Position(2, 1), new Cell(new Position(2, 1), true));
        cells.put(new Position(2, 2), new Cell(new Position(2, 2), true));

        grid = new Grid(3, 3, 2, cells);
    }

    @Test
    void should_change_state_to_EXPLODED_when_uncover_cell_with_mine() {
        //given
        Position aCellPosition = new Position(2, 1);
        //when
        Cell aCellUncovered = grid.uncoverACell(aCellPosition);
        //then
        assertThat(aCellUncovered).isNotNull();
        assertThat(aCellUncovered.getPosition()).isEqualTo(aCellPosition);
        assertThat(aCellUncovered.getState()).isEqualTo(CellState.EXPLODED);
    }

    @Test
    void should_change_state_to_UNCOVERED_when_uncover_cell_with_no_mine() {
        //given
        Position aCellPosition = new Position(0, 0);
        //when
        Cell aCellUncovered = grid.uncoverACell(aCellPosition);
        //then
        assertThat(aCellUncovered).isNotNull();
        assertThat(aCellUncovered.getPosition()).isEqualTo(aCellPosition);
        assertThat(aCellUncovered.getState()).isEqualTo(CellState.UNCOVERED);
    }

    @Test
    void should_throw_a_exception_if_position_is_outside_grid_when_uncover_cell() {
        //given
        Grid grid = new GridRandomBuilder().build(12, 10, 23);
        //then
        assertThatThrownBy(() -> grid.uncoverACell(new Position(12, 1)))
            .isInstanceOf(IllegalArgumentException.class);
    }

    @ParameterizedTest
    @CsvSource({"2,0", "1,0", "2,2", "2,1"})
    void should_return_one_mine_next_to_current_cell_when_uncover(int row, int column) {
        //when
        Cell aCellUncovered = grid.uncoverACell(new Position(row, column));
        //then
        assertThat(aCellUncovered.getNumberOfCellsWithMines())
            .isGreaterThan(Cell.UNKNOWN_NUMBER_OF_ADJACENT_CELLS_WITH_MINES)
            .isEqualTo(1);
    }

    @ParameterizedTest
    @CsvSource({"1,1", "1,2"})
    void should_return_two_mines_next_to_current_cell_when_uncover(int row, int column) {
        //when
        Cell aCellUncovered = grid.uncoverACell(new Position(row, column));
        //then
        assertThat(aCellUncovered.getNumberOfCellsWithMines())
            .isGreaterThan(Cell.UNKNOWN_NUMBER_OF_ADJACENT_CELLS_WITH_MINES)
            .isEqualTo(2);
    }

    @ParameterizedTest
    @CsvSource({"0,0", "0,1", "0,2"})
    void should_return_zero_mine_next_to_current_cell_when_uncover(int row, int column) {
        //when
        Cell aCellUncovered = grid.uncoverACell(new Position(row, column));
        //then
        assertThat(aCellUncovered.getNumberOfCellsWithMines())
            .isGreaterThan(Cell.UNKNOWN_NUMBER_OF_ADJACENT_CELLS_WITH_MINES)
            .isEqualTo(0);
    }

    @Test
    void should_uncover_all_adjencents_cells_when_uncover_a_cell_with_zero_adajcent_mine() {
        //when
        grid.uncoverACell(new Position(0, 0));
        //then
        assertCellStatus(0, 0, CellState.UNCOVERED, 0);
        assertCellStatus(0, 1, CellState.UNCOVERED, 0);
        assertCellStatus(0, 2, CellState.UNCOVERED, 0);
        assertCellStatus(1, 0, CellState.UNCOVERED, 1);
        assertCellStatus(1, 1, CellState.UNCOVERED, 2);
        assertCellStatus(1, 2, CellState.UNCOVERED, 2);
        assertCellStatus(2, 0, CellState.HIDDEN, Cell.UNKNOWN_NUMBER_OF_ADJACENT_CELLS_WITH_MINES);
        assertCellStatus(2, 1, CellState.HIDDEN, Cell.UNKNOWN_NUMBER_OF_ADJACENT_CELLS_WITH_MINES);
        assertCellStatus(2, 2, CellState.HIDDEN, Cell.UNKNOWN_NUMBER_OF_ADJACENT_CELLS_WITH_MINES);
    }

    private void assertCellStatus(int row, int column, CellState cellStateExpected, int numberOfMinesAroundExpected) {
        Optional<Cell> cell = grid.getACell(new Position(row, column));
        assertThat(cell).isPresent();
        assertThat(cell.get().getState()).isEqualTo(cellStateExpected);
        assertThat(cell.get().getNumberOfCellsWithMines()).isEqualTo(numberOfMinesAroundExpected);
    }
}
