package fr.julienke.kata.domain.entities;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class GridTest {

    @Test
    void should_get_a_cell_by_its_position() {
        //given
        Grid grid = new GridRandomBuilder().build(4, 12, 10);
        //when
        Optional<Cell> aSelectedCell = grid.getACell(new Position(2, 3));
        Optional<Cell> anotherSelectedCell = grid.getACell(new Position(3, 2));
        //then
        assertThat(aSelectedCell).isPresent();
        assertThat(anotherSelectedCell).isPresent();
        assertThat(aSelectedCell.get()).isNotEqualTo(anotherSelectedCell.get());
    }

    @ParameterizedTest
    @CsvSource({"2,3", "3,2", "-1,2", "2,-1"})
    void should_get_a_empty_cell_if_position_cell_is_outside_grid(int row, int column) {
        //given
        Grid grid = new GridRandomBuilder().build(3, 3, 5);
        //when
        Optional<Cell> aSelectedCell = grid.getACell(new Position(row, column));
        //then
        assertThat(aSelectedCell).isEmpty();
    }
}
