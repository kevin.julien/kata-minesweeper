package fr.julienke.kata.domain.useCase;

import fr.julienke.kata.domain.entities.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MinesweeperGameTest {

    @InjectMocks
    MinesweeperGame minesweeperGameMocked;

    @Mock
    Grid mockedGrid;

    @Test
    void should_generate_a_grid_when_initialize_a_game() {
        //given
        MinesweeperGame game = new MinesweeperGame();
        //when
        game.launch(4, 4, 10);
        //then
        assertThat(game.getGrid()).isNotNull();
    }

    @Test
    void should_uncovered_a_cell_and_increment_number_of_uncovered_cells_when_play() {
        //given
        Position positionExpected = new Position(3,2);
        Cell mockedCellWithNoMine = new Cell(positionExpected, false);
        when(mockedGrid.uncoverACell(eq(positionExpected))).thenReturn(mockedCellWithNoMine);
        //when
        minesweeperGameMocked.play(3,2);
        //then
        assertThat(mockedCellWithNoMine).isNotNull();
        assertThat(minesweeperGameMocked.getNumberOfUncoveredCells()).isEqualTo(1);
    }

    @Test
    void should_return_state_IN_PROGRESS_after_uncovered_a_cell_with_no_mine() {
        //given
        Position positionExpected = new Position(3,2);
        Cell mockedCellWithNoMine = new Cell(positionExpected, false);
        when(mockedGrid.uncoverACell(eq(positionExpected))).thenReturn(mockedCellWithNoMine);
        //when
        minesweeperGameMocked.play(3,2);
        //then
        verify(mockedGrid, times(1)).uncoverACell(positionExpected);
        assertThat(minesweeperGameMocked.getGameStatus()).isEqualTo(MinesweeperGameState.IN_PROGRESS);
    }

    @Test
    void should_return_state_FAIL_after_uncovered_a_cell_with_a_mine() {
        //given
        Position positionExpected = new Position(1,1);
        Cell mockedCellWithAMine = new Cell(positionExpected, true);
        when(mockedGrid.uncoverACell(eq(positionExpected))).thenReturn(mockedCellWithAMine);
        //when
        minesweeperGameMocked.play(1,1);
        //then
        verify(mockedGrid, times(1)).uncoverACell(positionExpected);
        assertThat(minesweeperGameMocked.getGameStatus()).isEqualTo(MinesweeperGameState.FAIL);
    }

    /**
     * Simulating a grid like that :
     *  [ ] [ ]
     *  [ ] [*]
     */
    @Test
    void should_return_state_WIN_after_uncovered_all_cells_with_no_mine() {
        //given
        when(mockedGrid.getColumn()).thenReturn(2);
        when(mockedGrid.getRow()).thenReturn(2);
        when(mockedGrid.getNumberOfMines()).thenReturn(1);
        Cell mockedCell = new Cell(new Position(1,1), false);
        when(mockedGrid.uncoverACell(any(Position.class))).thenReturn(mockedCell);
        //when
        minesweeperGameMocked.play(0,0);
        minesweeperGameMocked.play(0,1);
        minesweeperGameMocked.play(1,0);
        //then
        verify(mockedGrid, times(3)).uncoverACell(any(Position.class));
        assertThat(minesweeperGameMocked.getGameStatus()).isEqualTo(MinesweeperGameState.WIN);
    }

    @Test
    void should_throw_an_exception_if_play_on_a_terminated_game() {
        //given
        Position positionExpected = new Position(1,1);
        Cell mockedCell = new Cell(positionExpected, true);
        when(mockedGrid.uncoverACell(eq(positionExpected))).thenReturn(mockedCell);
        minesweeperGameMocked.play(1,1);
        //then
        assertThat(minesweeperGameMocked.getGameStatus()).isEqualTo(MinesweeperGameState.FAIL);
        assertThatThrownBy(() -> minesweeperGameMocked.play(1,1))
            .isInstanceOf(IllegalCallerException.class);
    }
}