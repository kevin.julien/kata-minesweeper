package fr.julienke.kata.domain.entities;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.IntStream;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public class GridRandomBuilder {

    public GridRandomBuilder() {
    }

    public Grid build(int numberOfRows, int numberOfColumns, int numberOfMines) {
        if(numberOfRows < 0 || numberOfColumns < 0) {
            throw new IllegalArgumentException("Number of rows or columns must be greater than 0");
        }
        if(numberOfMines > numberOfRows * numberOfColumns) {
            throw new IllegalArgumentException(format("Number of mines (%s) is greater than the size of the grid %s *" +
                " %s",numberOfMines, numberOfRows, numberOfColumns));
        }
        Map<Position, Cell> cells = generateCells(numberOfRows, numberOfColumns, numberOfMines);
        return new Grid(numberOfRows, numberOfColumns, numberOfMines, cells);
    }

    private Map<Position, Cell> generateCells(int numberOfRows, int numberOfColumns, int numberOfMines) {
        List<Integer> cellNumbersWithAMine = new Random()
            .ints(0, numberOfRows * numberOfColumns)
            .distinct()
            .limit(numberOfMines)
            .boxed()
            .collect(toList());

        return IntStream.range(0, numberOfRows)
            .boxed()
            .flatMap(currentRow -> IntStream.range(0, numberOfColumns)
                .boxed()
                .map(currentColumn -> generateCell(cellNumbersWithAMine, numberOfRows, currentRow, currentColumn))
            )
            .collect(toMap(Cell::getPosition, cell -> cell));
    }

    private Cell generateCell(List<Integer> cellNumbersWithAMine, int numberOfRow, int indexRow, int indexColumn) {
        int cellNumber = (numberOfRow*indexRow)+indexColumn;
        if (cellNumbersWithAMine.contains(cellNumber)) {
            return new Cell(new Position(indexRow, indexColumn), true);
        } else {
            return new Cell(new Position(indexRow, indexColumn), false);
        }
    }
}
