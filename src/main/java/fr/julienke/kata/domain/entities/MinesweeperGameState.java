package fr.julienke.kata.domain.entities;

public enum MinesweeperGameState {
    IN_PROGRESS, WIN, FAIL;
}
