package fr.julienke.kata.domain.entities;

public class Cell {

    public static final int UNKNOWN_NUMBER_OF_ADJACENT_CELLS_WITH_MINES = -1;

    private final Position position;

    private final boolean hasAMine;

    private CellState state;

    private int numberOfCellsWithMines;

    public Cell(Position position, boolean hasAMine) {
        this.position = position;
        this.hasAMine = hasAMine;
        this.state = CellState.HIDDEN;
        this.numberOfCellsWithMines = UNKNOWN_NUMBER_OF_ADJACENT_CELLS_WITH_MINES;
    }

    public boolean hasMine() {
        return hasAMine;
    }

    public CellState getState() {
        return state;
    }

    public Position getPosition() {
        return position;
    }

    public void uncover() {
        if(hasAMine){
            this.state = CellState.EXPLODED;
        } else {
            this.state = CellState.UNCOVERED;
        }
    }

    public int getNumberOfCellsWithMines() {
        return numberOfCellsWithMines;
    }

    public void setNumberOfCellsWithMines(int numberOfCellsWithMines) {
        this.numberOfCellsWithMines = numberOfCellsWithMines;
    }
}
