package fr.julienke.kata.domain.entities;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

public class Grid {

    private final Map<Position, Cell> cells;

    private final int row;

    private final int column;

    private final int numberOfMines;

    public Grid(int numberOfRow, int numberOfColumn, int numberOfMines, Map<Position, Cell> cells) {
        this.row = numberOfRow;
        this.column = numberOfColumn;
        this.numberOfMines = numberOfMines;
        this.cells = cells;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public Map<Position, Cell> getCells() {
        return cells;
    }

    public Optional<Cell> getACell(Position position) {
        if (cells.get(position) == null) {
            return Optional.empty();
        }
        return Optional.of(cells.get(position));
    }

    public int getNumberOfMines() {
        return numberOfMines;
    }

    public Cell uncoverACell(Position position) {
        Optional<Cell> optSelectedCell = this.getACell(position);
        Cell selectedCell = optSelectedCell.orElseThrow(() ->
            new IllegalArgumentException(format("Cell with position row: %s, column: %s is impossible",
                position.getRow(),
                position.getColumn())
            ));
        selectedCell.uncover();
        int numberOfAdjacentMines = this.determineNumberOfMinesNextToACell(selectedCell);
        selectedCell.setNumberOfCellsWithMines(numberOfAdjacentMines);

        if (numberOfAdjacentMines == 0) {
            this.getAdjacentCell(selectedCell)
                .stream()
                .filter(cell -> CellState.HIDDEN.equals(cell.getState()))
                .forEach(cell -> uncoverACell(cell.getPosition()));
        }
        return selectedCell;
    }

    private int determineNumberOfMinesNextToACell(Cell selectedCell) {
        return (int) this.getAdjacentCell(selectedCell)
            .stream()
            .filter(Cell::hasMine)
            .count();
    }

    private List<Cell> getAdjacentCell(Cell selectedCell) {
        return IntStream.rangeClosed(-1, 1)
            .boxed()
            .flatMap(row -> IntStream.rangeClosed(-1, 1)
                .boxed()
                .map(column -> {
                    int adjacentRow = selectedCell.getPosition().getRow() + row;
                    int adjacentColumn = selectedCell.getPosition().getColumn() + column;
                    Position adjacentPosition = new Position(adjacentRow, adjacentColumn);
                    return getACell(adjacentPosition);
                }))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .filter(cell -> cell.getPosition() != selectedCell.getPosition())
            .collect(toList());
    }
}
