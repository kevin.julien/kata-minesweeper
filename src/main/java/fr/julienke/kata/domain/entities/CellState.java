package fr.julienke.kata.domain.entities;

public enum CellState {
    UNCOVERED,
    EXPLODED,
    HIDDEN
}
