package fr.julienke.kata.domain.entities;

import fr.julienke.kata.utils.Generated;

import java.util.Objects;

public class Position {

    private final int row;

    private final int column;

    public Position(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }


    @Generated
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return row == position.row && column == position.column;
    }

    @Generated
    @Override
    public int hashCode() {
        return Objects.hash(row, column);
    }
}
