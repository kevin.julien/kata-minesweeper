package fr.julienke.kata.domain.useCase;

import fr.julienke.kata.domain.entities.*;

import static java.lang.String.format;

public class MinesweeperGame {

    private Grid grid;

    private int numberOfUncoveredCells = 0;

    private MinesweeperGameState gameStatus =  MinesweeperGameState.IN_PROGRESS;

    public Grid getGrid() {
        return grid;
    }

    public int getNumberOfUncoveredCells() {
        return numberOfUncoveredCells;
    }

    public MinesweeperGameState getGameStatus() {
        return gameStatus;
    }

    public void launch(int numberOfRows, int numberOfColumns, int numberOfMines) {
        grid = new GridRandomBuilder().build(numberOfRows, numberOfColumns, numberOfMines);
    }

    public Cell play(int row, int column) {
        if(!MinesweeperGameState.IN_PROGRESS.equals(gameStatus)) {
            throw new IllegalCallerException(format("Cannot play on a game done. State : %s", gameStatus.toString()));
        }
        Cell uncoveredCell =  grid.uncoverACell(new Position(row, column));
        calculateGameState(uncoveredCell);
        return uncoveredCell;
    }

    private void calculateGameState(Cell uncoveredCell) {
        if(uncoveredCell.hasMine()) {
            gameStatus = MinesweeperGameState.FAIL;
        } else {
            numberOfUncoveredCells++;
            if(hasUncoveredAllCellsWithNoMines()) {
                gameStatus = MinesweeperGameState.WIN;
            }
        }
    }

    private boolean hasUncoveredAllCellsWithNoMines() {
        return numberOfUncoveredCells == grid.getColumn() * grid.getRow() - grid.getNumberOfMines();
    }
}
